# SpringBoot-Thread
> 使用SpringBoot练习多线程的使用
-- -- 

# 学习重点
## 1、异步线程使用
- 异步线程-简单使用
- 异步线程-传参使用
- 异步线程-mockito
- 异步线程-异常的其他捕获方式-主线程版
- 异步线程-异常的其他捕获方式-子线程版
> 参考网址：https://blog.csdn.net/justry_deng/article/details/105877524

