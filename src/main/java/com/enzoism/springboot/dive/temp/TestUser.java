package com.enzoism.springboot.dive.temp;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TestUser {
    private String usrName;
    private Integer usdId;
}
