package com.enzoism.springboot.dive.temp;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 1.[对象排序传参]：https://blog.csdn.net/leiyong0326/article/details/52035599?locationNum=4&fps=1
 * 2.
 */
@Slf4j
public class TempMain {
    public static void main(String[] args) {
        List<TestUser> list = initList();
        // 1-list根据属性分组
//        Map<Integer, List<TestUser>> groupMap = list.stream().collect(Collectors.groupingBy(item -> item.getUsdId()));
//        log.info(groupMap.toString());

        // 2-list根据拼接属性分组
//        Map<String, List<TestUser>> jointGroupMap = list.stream().collect(Collectors.groupingBy(item -> item.getUsrName() + item.getUsdId()));
//        log.info(jointGroupMap.toString());

        // 3-list根据条件分组-有点类似
//        Map<String, List<TestUser>> conGroupMap = list.stream().collect(Collectors.groupingBy(item -> {
//                    if (item.getUsdId() > 5) {
//                        return "minNum";
//                    } else {
//                        return "maxNum";
//                    }
//                }
//        ));
//        log.info(conGroupMap.toString());

        // 3-list根据条件分类
//        Map<Boolean, List<TestUser>> partGroupMap = list.stream().collect(Collectors.partitioningBy(item -> item.getUsdId() > 5));
//        log.info(partGroupMap.toString());


        // 4-组合条件
//        List<TestClass> classList = TestClass.initList();
//        Map<String, Map<String, List<TestClass>>> multiGroupMap = classList.stream().collect(Collectors.groupingBy(TestClass::getClassType,
//                Collectors.groupingBy(item -> {
//                    int score = item.getScore();
//                    if (score >= 85) {
//                        return "高级";
//                    } else if (score < 85 && score >= 60) {
//                        return "中级";
//                    } else {
//                        return "低级";
//                    }
//                }
//        )));
//        log.info(multiGroupMap.toString());

        // 5-按子集收集结果-求和
//        List<TestClass> classList = TestClass.initList();
//        Map<String, Integer> prodMap = classList.stream().collect(Collectors.groupingBy(TestClass::getClassType, Collectors.summingInt(TestClass::getScore)));
//        log.info(prodMap.toString());

        // 5-按子集收集结果-计数
//        List<TestClass> classList = TestClass.initList();
//        Map<String, Long> prodMap = classList.stream().collect(Collectors.groupingBy(TestClass::getClassType, Collectors.counting()));
//        log.info(prodMap.toString());

        // 5-按子集收集结果-把收集器的结果转换为另一种类型(取出最大的数据对象)
//        List<TestClass> classList = TestClass.initList();
//        Map<String, TestClass> prodMap = classList.stream().collect(Collectors.groupingBy(TestClass::getClassType,
//                Collectors.collectingAndThen(Collectors.maxBy(Comparator.comparingInt(TestClass::getScore)), Optional::get)));
//        log.info(prodMap.toString());

        // 5-按子集收集结果-收集分组的结果
        List<TestClass> classList = TestClass.initList();
        Map<String, Set<Integer>> prodMap = classList.stream().collect(Collectors.groupingBy(TestClass::getClassType,
                Collectors.mapping(TestClass::getScore, Collectors.toSet())));
        log.info(prodMap.toString());

        // 6- 测试数组
        /**
         * 测试-
         * 1-打印结果是0还是null?
         * 2-可以调用array[1]方法吗？
         *
         */
        String[] array = new String[5];


    }

    public static List<TestUser> initList() {
        List<TestUser> list = new ArrayList<>();
        list.add(new TestUser("001", 001));
        list.add(new TestUser("002", 002));
        list.add(new TestUser("003", 003));
        list.add(new TestUser("004", 004));
        list.add(new TestUser("005", 005));
        list.add(new TestUser("006", 006));
        list.add(new TestUser("007", 007));
        return list;
    }
}
