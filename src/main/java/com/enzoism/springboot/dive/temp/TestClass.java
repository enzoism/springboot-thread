package com.enzoism.springboot.dive.temp;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
public class TestClass {
    private String classType;
    private Integer score;

    public static List<TestClass> initList() {
        List<TestClass> list = new ArrayList<>();
        list.add(new TestClass("语文", 34));
        list.add(new TestClass("英语", 23));
        list.add(new TestClass("语文", 59));
        list.add(new TestClass("语文", 85));
        list.add(new TestClass("英语", 55));
        list.add(new TestClass("英语", 65));
        list.add(new TestClass("英语", 78));
        return list;
    }
}
