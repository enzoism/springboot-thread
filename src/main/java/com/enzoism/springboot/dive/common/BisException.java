package com.enzoism.springboot.dive.common;

/**
 * 自定义业务异常
 */
public class BisException extends RuntimeException {
    public BisException(String message) {
        super(message);
    }
}
