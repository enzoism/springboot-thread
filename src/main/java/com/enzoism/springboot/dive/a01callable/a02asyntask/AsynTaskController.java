package com.enzoism.springboot.dive.a01callable.a02asyntask;

import com.enzoism.springboot.dive.result.RequestResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 测试异步线程结果的获取-复杂版
 * 1）Callable需要进行参数对象传递(参数的可能是个外部接口)
 * 2）注意：编写Handler将需要【再加工】的方法进行拆解出来
 * 3）注意：最好的是将take().get()方法编写一个单独的方法-后续可以单独进行异常处理
 * 4）注意：线程池创建后要进行注销：executorService.shutdown();
 * 5）验证：http://localhost:8075/asyn/api
 */
@RequestMapping("asyn")
@RestController
public class AsynTaskController {

    @Autowired
    private AsynTaskServiceHelper asynTaskServiceHelper;

    @GetMapping("/api")
    public RequestResult getRestApiForOtherSystem() {
        List<String> aSynRestApiListResult = asynTaskServiceHelper.getASynRestApiListResult();
        return RequestResult.success(aSynRestApiListResult);
    }
}
