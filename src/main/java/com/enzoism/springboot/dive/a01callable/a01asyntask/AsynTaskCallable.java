package com.enzoism.springboot.dive.a01callable.a01asyntask;

import java.util.Random;
import java.util.concurrent.Callable;

/**
 * 进行异步结构的返回-简单版
 */
public class AsynTaskCallable implements Callable<Integer> {
    @Override
    public Integer call() throws Exception {
        Random random = new Random();
        Thread.sleep(random.nextInt(1000) + 500);
        return random.nextInt(1000);
    }
}
