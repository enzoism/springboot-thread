package com.enzoism.springboot.dive.a01callable.a03asynexception;

import com.enzoism.springboot.dive.a01callable.a02asyntask.AsynTaskService;
import com.enzoism.springboot.dive.common.BisConstant;
import com.enzoism.springboot.dive.common.BisException;

import java.util.Random;
import java.util.concurrent.Callable;

/**
 * 进行异步结构的返回
 */
public class AsynTaskExceptionCallable implements Callable<String> {
    private String name;
    private AsynTaskService asynTaskService;

    public AsynTaskExceptionCallable(String name, AsynTaskService asynTaskService) {
        this.name = name;
        this.asynTaskService = asynTaskService;
    }

    @Override
    public String call() throws Exception {
        int num = new Random().nextInt(100);
        System.out.println("-----num%3="+num%3);
        if (num%3==0){
            throw new BisException(BisConstant.ERROR_FILTER_MSG);
        }
        // 模拟异步请求外部接口
        return asynTaskService.getRestApiForOtherSystem("系统编号00" + num);
    }
}
