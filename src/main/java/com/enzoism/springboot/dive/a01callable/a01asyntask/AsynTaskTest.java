package com.enzoism.springboot.dive.a01callable.a01asyntask;

import java.util.concurrent.*;

/**
 * 测试异步线程结果的获取
 */
public class AsynTaskTest {

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        // 创建异步线程池
        int threadSize = 5;
        ExecutorService executorService = Executors.newFixedThreadPool(threadSize);
        CompletionService<Integer> completionService = new ExecutorCompletionService(executorService);

        // 进行异步请求
        for (int i = 0; i < threadSize; i++) {
            completionService.submit(new AsynTaskCallable());
        }

        // 进行异步结果获取
        for (int i = 0; i < threadSize; i++) {
            System.out.println("----->异步线程执行结果：" + completionService.take().get());
        }

        // 线程池销毁
        executorService.shutdown();
    }
}
