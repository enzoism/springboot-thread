package com.enzoism.springboot.dive.a01callable.a02asyntask;

import org.springframework.stereotype.Service;

import java.util.Random;

/**
 * 单纯的进行rest的外部接口调用
 */
@Service
public class AsynTaskService {
    /**
     * 简单模拟rest接口的外部调用/数据库的真实操作
     */
    public String getRestApiForOtherSystem(String str) throws InterruptedException {
        int time = new Random().nextInt(1000) + 500;
        Thread.sleep(time);
        return str + "耗时：" + time + "毫秒";
    }
}
