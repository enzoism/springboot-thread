package com.enzoism.springboot.dive.a01callable.a04asynexception2;

import com.enzoism.springboot.dive.common.BisConstant;
import com.enzoism.springboot.dive.common.BisException;

import java.util.Random;
import java.util.concurrent.Callable;

/**
 * 进行异步结构的返回
 * 1）简化编程逻辑
 * 2）适使用子线程记进行异常处理
 */
public class AsynTaskExceptionCallable2 implements Callable<String> {
    private String name;
    private AsynTaskExceptionService2 asynTaskExceptionService2;

    public AsynTaskExceptionCallable2(String name, AsynTaskExceptionService2 asynTaskExceptionService2) {
        this.name = name;
        this.asynTaskExceptionService2 = asynTaskExceptionService2;
    }

    @Override
    public String call() throws Exception {
        // 模拟异步请求外部接
        String result;
        try {
            result=  asynTaskExceptionService2.getRestApiForOtherSystem("系统编号00" + new Random().nextInt(100));
        } catch (BisException e){
            if (e.getMessage().contains(BisConstant.ERROR_FILTER_MSG)) {
                result = "捕获异常后进行返回值替换";
                return result;
            }
            e.printStackTrace();
            result = "捕获异常后进行返回值替换";
        }
        return  result;
    }
}
