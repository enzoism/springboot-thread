package com.enzoism.springboot.dive.a01callable.a02asyntask;

import java.util.Random;
import java.util.concurrent.Callable;

/**
 * 进行异步结构的返回
 */
public class AsynTaskCallable implements Callable<String> {
    private String name;
    private AsynTaskService asynTaskService;

    public AsynTaskCallable(String name, AsynTaskService asynTaskService) {
        this.name = name;
        this.asynTaskService = asynTaskService;
    }

    @Override
    public String call() throws Exception {
        // 模拟异步请求外部接口
        return asynTaskService.getRestApiForOtherSystem("系统编号00" + new Random().nextInt(100));
    }
}
