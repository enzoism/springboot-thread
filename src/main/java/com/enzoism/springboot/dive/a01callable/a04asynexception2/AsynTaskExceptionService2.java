package com.enzoism.springboot.dive.a01callable.a04asynexception2;

import com.enzoism.springboot.dive.common.BisConstant;
import com.enzoism.springboot.dive.common.BisException;
import org.springframework.stereotype.Service;

import java.util.Random;

/**
 * 单纯的进行rest的外部接口调用
 */
@Service
public class AsynTaskExceptionService2 {
    /**
     * 简单模拟rest接口的外部调用/数据库的真实操作
     */
    public String getRestApiForOtherSystem(String str) throws InterruptedException {
        int num = new Random().nextInt(1000) + 500;
        System.out.println("-----rest服务进行数据外部数据请求：" + num % 3);
        if (num % 3 == 0) {
            throw new BisException(BisConstant.ERROR_FILTER_MSG);
        }
        Thread.sleep(num);
        return str + "耗时：" + num + "毫秒";
    }
}
