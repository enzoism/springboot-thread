package com.enzoism.springboot.dive.a01callable.a05asyafterexecute;

import com.enzoism.springboot.dive.a01callable.a04asynexception2.AsynTaskExceptionCallable2;
import com.enzoism.springboot.dive.a01callable.a04asynexception2.AsynTaskExceptionService2;
import com.enzoism.springboot.dive.common.BisException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * 程序设计的时候应该敏感的将【数据再加工方法】放到外部进行操作，否则不好Mock
 * 1）使用AsynTaskService进行外部接口调用
 * 2）使用callable进行异步数据的获取
 * 3）进行特性异常的捕获和处理
 * 4）异步线程的异常会被ExecutionException捕获，然后可以替换返回值
 */
@Service
public class AsynTaskServiceAfterExecuteHelper {
    @Autowired
    private AsynTaskExceptionService2 asynTaskExceptionService2;

    /**
     * 简单模拟rest接口的外部调用/数据库的真实操作
     */
    public List<String> getASynRestApiListResult() {

        // 创建异步线程池
        int threadSize = 5;
        ExecutorService executorService = new MyThreadPoolExecutor(threadSize, threadSize,
                0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<Runnable>());
        CompletionService<String> completionService = new ExecutorCompletionService(executorService);

        // 进行异步请求
        for (int i = 0; i < threadSize; i++) {
            completionService.submit(new AsynTaskExceptionCallable2("系统编号00" + i, asynTaskExceptionService2));
        }

        // 进行异步结果获取
        List<String> list = new ArrayList<>();
        for (int i = 0; i < threadSize; i++) {
            String str = getASynRestApiFromCompletionService(completionService);
            System.out.println("----->异步线程执行结果：" + str);
            list.add(str);
        }

        // 线程池销毁
        executorService.shutdown();

        return list;
    }

    private String getASynRestApiFromCompletionService(CompletionService<String> completionService) {
        try {
            return completionService.take().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
            Thread.currentThread().interrupt();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        throw new BisException("程序出错");
    }
}
