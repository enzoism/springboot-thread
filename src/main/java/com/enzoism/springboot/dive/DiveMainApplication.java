package com.enzoism.springboot.dive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DiveMainApplication {

    public static void main(String[] args) {
        SpringApplication.run(DiveMainApplication.class, args);
    }

}
